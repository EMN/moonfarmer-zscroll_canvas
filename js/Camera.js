

function Camera(){


    this.scrollSpeed = 1;
    this.cameraOutput = document.getElementById("cameraOutput");
    this.progressbar = document.getElementById("progressbar");
    this.progress = 0;
    this.waypoints = [0,414,420,444,471];
    this.start = this.waypoints[0];
    this.end = this.waypoints[this.waypoints.length-1];
    this.x = 0.0;
    this.y = 0.0;
    this.z = this.start;
    this.newz = 0.0;
    this.moving = false;
    this.easeIteration=0;
    this.easeTotalIteration=90;
    this.easeStart = 0;
    this.waypointIndex = 0;

    this.draw = function(){
        this.z = ((this.z < this.start)?this.z = this.start : this.z);
        this.z = ((this.z > this.end)?this.z = this.end : this.z);
        mat4.translate(mvMatrix, [-this.x, this.y, this.z]);

        this.triggers();
        this.output();

        if(this.moving){
            if(this)

            this.z = easeOutQuint(this.easeIteration++,this.easeStart,this.newz, this.easeTotalIteration);

            if(this.easeIteration >= this.easeTotalIteration){
                this.moving = false;
            }
        }
    };//..

    this.triggers = function(){
        // if(parseInt(this.progress) > 95){
        //     barn.moveTox(0);
        // }
    };//..

    this.output = function(){
        this.cameraOutput.innerHTML = "<b>Camera:</b><br>" +
        "<b>X: "+this.x+" Y: "+this.y+" Z: "+this.z+" </b>";
        this.progress = ((this.z / this.end)*100)+"%";
        this.progressbar.style.width = this.progress;
        this.progressbar.innerHTML = parseInt(this.progress)+"%";
    };//..

    this.scroll = function(e) {
        // if(e.deltaY > 0) {
        //     this.waypointIndex = (this.waypointIndex+1 < this.waypoints.length) ? this.waypointIndex+= 1 : this.waypointIndex;
        // }else {
        //     this.waypointIndex = (this.waypointIndex-1 >= 0) ? this.waypointIndex-= 1 : this.waypointIndex;
        // }
        // this.moveToWaypoint(this.waypointIndex);
        if(this.z + e.deltaY*2 > this.start 
            && this.z + e.deltaY*2 < this.end )
                this.moveTo(this.z += e.deltaY*2);

    };//..

    this.moveTo= function(newz){

            this.moving = true;
            this.newz = newz-this.z;
            this.easeIteration = 0;
            this.easeTotalIteration = 100;
            this.easeStart = this.z;
    };//..

    this.moveToWaypoint= function(idx){
        this.waypointIndex = idx;
        this.moving = true;
        this.newz = this.waypoints[idx]-this.z;
        this.easeIteration = 0;
        this.easeTotalIteration = 100;
        this.easeStart = this.z;
    };//..

}//..