

//alert("Cube");

function Plane(x,y,z,texture){

    this.x = x;
    this.y = y;
    this.z = z;
    this.xrot = 0;
    this.yrot = 0;
    this.zrot = 0;
    this.dxrot = 0;
    this.dyrot = 0;
    this.dzrot = 0;
    this.scaleVec = [1,1,1];
    this.texture = texture;
    this.extraLogic = function(){};

    //movement
    this.newz = 0.0;
    this.moving = false;
    this.easeIteration=0;
    this.easeTotalIteration=90;
    this.easeStart = 0;

    this.VertexPositionBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexPositionBuffer);
    this.vertices = [
        // Front face
        -0.5, 0.5, 0.0,
         0.5, 0.5, 0.0,
         0.5,-0.5, 0.0,
        -0.5,-0.5, 0.0

    ];
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.vertices), gl.STATIC_DRAW);
    this.VertexPositionBuffer.itemSize = 3;
    this.VertexPositionBuffer.numItems = 12;

    this.VertexTextureCoordBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexTextureCoordBuffer);
    this.textureCoords = [
        // Front face
        0.0, 0.0,
        1.0, 0.0,
        1.0, 1.0,
        0.0, 1.0
    ];
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.textureCoords), gl.STATIC_DRAW);
    this.VertexTextureCoordBuffer.itemSize = 2;
    this.VertexTextureCoordBuffer.numItems = 8;

    this.VertexNormalBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexNormalBuffer);
    this.vertexNormals = [
        // Front face
        0.0,  0.0,  1.0,
        0.0,  0.0,  1.0,
        0.0,  0.0,  1.0,
        0.0,  0.0,  1.0
    ];
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.vertexNormals), gl.STATIC_DRAW);
    this.VertexNormalBuffer.itemSize = 3;
    this.VertexNormalBuffer.numItems = 12;

    this.VertexIndexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.VertexIndexBuffer);
    this.VertexIndices = [
        0, 1, 2,      0, 2, 3
    ];
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(this.VertexIndices), gl.STATIC_DRAW);
    this.VertexIndexBuffer.itemSize = 1;
    this.VertexIndexBuffer.numItems = 6;

    this.draw = function(){
        pushMatrix();

        this.extraLogic();

        if(this.moving){
            this.x = easeOutQuint(this.easeIteration++,this.easeStart,this.newz, this.easeTotalIteration);
            if(this.easeIteration >= this.easeTotalIteration){
                this.moving = false;
            }
        }

        mat4.translate(mvMatrix, [this.x, this.y, this.z]);

        mat4.rotate(mvMatrix, degToRad(this.xrot), [1, 0, 0]);
        mat4.rotate(mvMatrix, degToRad(this.yrot), [0, 1, 0]);
        mat4.rotate(mvMatrix, degToRad(this.zrot), [0, 0, 1]);

        mat4.scale(mvMatrix,this.scaleVec);

        this.xrot+=this.dxrot;
        this.yrot+=this.dyrot;
        this.zrot+=this.dzrot;

        gl.bindBuffer(gl.ARRAY_BUFFER,  this.VertexPositionBuffer);
        gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute,  this.VertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

        //gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexNormalBuffer);
        //gl.vertexAttribPointer(shaderProgram.vertexNormalAttribute, this.VertexNormalBuffer.itemSize, gl.FLOAT, false, 0, 0);

        gl.bindBuffer(gl.ARRAY_BUFFER,  this.VertexTextureCoordBuffer);
        gl.vertexAttribPointer(shaderProgram.textureCoordAttribute,  this.VertexTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);


        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, this.texture);
        gl.uniform1i(shaderProgram.samplerUniform, 0);



        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER,  this.VertexIndexBuffer);
        setMatrixUniforms();
        gl.drawElements(gl.TRIANGLES,  this.VertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
        popMatrix();
    };//..

    this.moveToZ= function(newz){

            this.moving = true;
            this.newz = newz-this.z;
            this.easeIteration = 0;
            this.easeTotalIteration = 100;
            this.easeStart = this.z;
    };//..

    this.moveToX= function(newx){

            this.moving = true;
            this.newx = newx-this.x;
            this.easeIteration = 0;
            this.easeTotalIteration = 100;
            this.easeStart = this.x;
    };//..

}// Triangle



